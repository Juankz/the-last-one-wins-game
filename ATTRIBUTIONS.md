# Attributions

## Graphics

HDRI kitchen is a derivation from "Low Poly Kitchen" (https://skfb.ly/ov6PR) by EdwinRC which is licensed under Creative Commons Attribution
 (http://creativecommons.org/licenses/by/4.0/).

## Audio

Sound files from freesound.org

- Hitting in a Face by florianreichelt | License: Creative Commons 0
- Tearing, Newspaper, A.wav by InspectorJ | License: Attribution 4.0
- Dark Jazz Pattern by tripjazz | License: Attribution 4.0
- Race Start Countdown by JustInvoke | License: Attribution 4.0
- blender.wav by juanpabloagredo | License: Creative Commons 0
- Falling fruit / Drumming on cookie box by lucasfuchs | License: Creative Commons 0
- JazzBox by tripjazz | License: Attribution 4.0

Click sound by kenney.nl

## Code

This game uses Godot Engine, available under the following license:

Copyright (c) 2014-present Godot Engine contributors. Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
