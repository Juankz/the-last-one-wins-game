# The last one wins

A racing videogame where the last one wins. Made for the linux game jam 2023

# Requirements

1. Git-lfs
1. Godot v3.5.2

# Licenses:
3D models are licensed under a [CC BY-NC 4.0 license](https://creativecommons.org/licenses/by-nc/4.0/)

Source code is licensed under the [Mozilla Public License version 2](https://www.mozilla.org/en-US/MPL/2.0/)

For other licenses see the ATTRIBUTIONS.md file
