extends Resource
class_name CharacterSkins

enum SKINS {
	LEAVES,
	HAT,
	GLASSES,
	HEADPHONES
}

export(SKINS) var skin = SKINS.LEAVES
