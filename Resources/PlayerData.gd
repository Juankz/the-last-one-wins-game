# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Resource
class_name PlayerData

export(int) var playerId = 1
export(CharacterSkins.SKINS) var skin = 0
