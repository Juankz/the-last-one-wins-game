# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Camera

const MAX_Y: float = 0.5
const MIN_Y: float = -0.5
var d: Vector3
var dist: float
var dir: Vector3
onready var parent = get_parent()

func _ready():
	set_as_toplevel(true)
	d = global_translation - parent.global_translation
	dir = d.normalized()
	dist = d.length()

func _process(delta):
	var desired_pos = get_parent().global_translation + (dir * dist)
	var diff = desired_pos - global_translation
	var interpolated_pos = global_translation.linear_interpolate(desired_pos, delta)
	var new_dir = parent.global_translation.direction_to(interpolated_pos)
	var new_pos = new_dir * dist
	new_pos.y = clamp(new_pos.y, (dir*dist).y + MIN_Y, (dir*dist).y + MAX_Y)
	global_translation = parent.global_translation + new_pos
