# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Spatial

const SPEED = 10
const MAX_DIST = 1
var distanceToTarget: float
var prevTargetPos: Vector3
var desiredTransform: Transform

onready var target: Spatial = get_parent()

func _ready():
	set_as_toplevel(true)
	desiredTransform = transform
	prevTargetPos = target.translation


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var targetPos = target.translation
	var variation = prevTargetPos.direction_to(targetPos)
	var desiredLookAt = translation + Vector3(variation.x, 0, variation.z).normalized()
	if(desiredLookAt != translation):
		desiredTransform = transform.looking_at(desiredLookAt, Vector3.UP)
	transform = transform.interpolate_with(desiredTransform, delta)
	translation = target.translation
	prevTargetPos = targetPos
