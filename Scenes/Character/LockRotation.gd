# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Spatial

func _ready():
	set_as_toplevel(true)

func _process(delta):
	global_translation = get_parent().global_translation
