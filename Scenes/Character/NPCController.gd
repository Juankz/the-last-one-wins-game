# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Controller
class_name NPCController

var body: RigidBody
var obstacles: Array
var prev_pos: Vector3

func _init(b: RigidBody):
	body = b
	prev_pos = body.global_translation
	obstacles = body.get_tree().get_nodes_in_group("obstacles")

func get_closest_obstacle_pos() -> Vector3:
	assert(obstacles.size() > 0)
	var v = Vector3()
	var dist = body.global_translation.distance_to((obstacles[0] as Spatial).global_translation)
	for obstacle in obstacles:
		var obs_dist = body.global_translation.distance_to((obstacle as Spatial).global_translation)
		if obs_dist < dist:
			dist = obs_dist
			v = (obstacle as Spatial).global_translation
	return v

func get_controller_velocity() -> Vector3:
	var v = Vector3()
	if obstacles.size() > 0:
		var target_pos = get_closest_obstacle_pos()
		v = target_pos - body.global_translation
		v.x = sign(v.x)
	return v
