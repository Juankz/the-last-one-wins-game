# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends RigidBody
class_name Orange

# Control 
export var player_controlled: bool = false
var controller: Controller = Controller.new()

# Movement
var currentVelocity: Vector3 = Vector3();
var player_data: PlayerData = null
onready var initialTranslation = translation
onready var camera = $CameraRig/Camera

# Character
var leavesDecorator = preload("res://Assets/Orange/Faces/leaves.glb")
var hatDecorator = preload("res://Assets/Orange/Faces/hat.glb")
var headphonesDecorator = preload("res://Assets/Orange/Faces/headphones.glb")
var glassesDecorator = preload("res://Assets/Orange/Faces/glasses.glb")

# Called when the node enters the scene tree for the first time.
func _ready():
	assert(player_data != null, "Player data is null") 
	
	# Appereance setup
	$Decorators.set_as_toplevel(true) # Decorators don't rotate
	add_skin(player_data.skin)
	
	# Connect physics signals
	$Area.connect("body_entered", self, "_on_Orange_body_entered")
	connect("body_entered", self, "_on_body_collision")
	
	# Define a controller
	if player_controlled:
		controller = PlayerController.new(player_data)
		camera.current = true
		$Listener.current = true
#		gravity_scale = 1.4
		$HUD.visible = true
		$HUD/PlayerID.text = str("P", player_data.playerId)
	else:
		$HUD.visible = false
		controller = NPCController.new(self)
#		linear_damp = 0.7
		
	# Start sleeping
	sleeping = true

func _process(delta):
	if sleeping: return
	
	# Update decorator translation
	$Decorators.translation = translation
	
	# Always apply an impulse forwards
	var auto_velocity = camera.transform.basis.z * -1 * 0.005
	apply_central_impulse(auto_velocity)
	
	# Apply an impulse based on the controller
	var controller_velocity = camera.transform.basis.x * controller.get_controller_velocity().x
	apply_central_impulse(controller_velocity.normalized() * 0.01)
	
	$AudioFalling.unit_db = get_volume_by_velocity()

# Area collision. Convert all bodies of an obstacle from static to rigid body
func _on_Orange_body_entered(body: Node):
	if body.is_in_group("obstacles"):
#		if ((body as RigidBody).mode == RigidBody.MODE_STATIC):
		$AudioCrash.play()
		var siblings = body.get_parent().get_children()
		for node in siblings:
			if node is RigidBody:
				node.mode = RigidBody.MODE_RIGID

# Body collision
func _on_body_collision(body: Node):
	if body.is_in_group("obstacles"):
		# Change the collsion layer after 1 second to avoid characters getting stuck
		yield(get_tree().create_timer(1.0), "timeout")
		body.collision_layer = 2
		

func get_volume_by_velocity() -> float:
	var v = clamp((linear_velocity.length() - 5) * 4, -20, 20)
	return v

func add_skin(skin):
	var current_skin: Node
	match skin:
		CharacterSkins.SKINS.LEAVES:
			current_skin = leavesDecorator.instance()
		CharacterSkins.SKINS.HAT:
			current_skin = hatDecorator.instance()
		CharacterSkins.SKINS.HEADPHONES:
			current_skin = headphonesDecorator.instance()
		CharacterSkins.SKINS.GLASSES:
			current_skin = glassesDecorator.instance()
	if current_skin:
		for child in $Decorators/Skin.get_children():
			child.queue_free()
		$Decorators/Skin.add_child(current_skin)

func awake():
	sleeping = false
	$AudioFalling.play()
	
func set_as_winner():
	$HUD/WinMessage.visible = true

func set_as_loser():
	$HUD/WinMessage.text = "You lose"
	$HUD/WinMessage.visible = true
