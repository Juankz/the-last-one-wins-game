# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Spatial

var skin = CharacterSkins.SKINS.LEAVES
export var playerID: int = 1
onready var current_skin: Spatial = $Decorators/leaves

func _ready():
	set_skin(skin)

func set_skin(new_skin):
	current_skin.visible = false
	match new_skin:
		CharacterSkins.SKINS.LEAVES:
			current_skin = $Decorators/leaves
		CharacterSkins.SKINS.HAT:
			current_skin = $Decorators/hat
		CharacterSkins.SKINS.HEADPHONES:
			current_skin = $Decorators/headphones
		CharacterSkins.SKINS.GLASSES:
			current_skin = $Decorators/glasses
			
	current_skin.visible = true
	skin = new_skin
	
	# Update global information
	PlayersInfo.players_data[playerID - 1].skin = skin
	
func next_character():
	if skin < CharacterSkins.SKINS.size() - 1:
		set_skin(skin + 1)
	else:
		set_skin(0)

func prev_character():
	if skin > 0:
		set_skin(skin - 1)
	else:
		set_skin(CharacterSkins.SKINS.size() - 1)

func _unhandled_input(event):
	if event.is_action_pressed(str("player",playerID,"_left")):
		prev_character()
	if event.is_action_pressed(str("player",playerID,"_right")):
		next_character()
