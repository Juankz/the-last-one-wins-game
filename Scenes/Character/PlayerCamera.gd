extends Camera

const SPEED = 10
const MAX_DIST = 1
var distanceToTarget: float
var anchor: Vector3
var prevTargetPos: Vector3

onready var target: Spatial = get_parent()

func _ready():
	set_as_toplevel(true)
	distanceToTarget = translation.distance_to(target.translation)
	anchor = translation - target.translation
	prevTargetPos = target.translation


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var targetPos = target.translation
	var variation = prevTargetPos.direction_to(targetPos)
	var desiredTransform = transform.looking_at(translation + Vector3(variation.x, 0, variation.z).normalized(), Vector3.UP)
#	transform = transform.interpolate_with(desiredTransform, delta)
	
	translation = targetPos + (transform.basis.z * distanceToTarget)
	prevTargetPos = targetPos
