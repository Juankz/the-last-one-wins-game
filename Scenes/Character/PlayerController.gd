# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Controller
class_name PlayerController

var playerID = 1

func _init(playerData: PlayerData):
	playerID = playerData.playerId

func get_controller_velocity() -> Vector3:
	var velocity = Vector3()
	if Input.is_action_pressed(str("player",playerID,"_left")):
		velocity.x = -1
	if Input.is_action_pressed(str("player",playerID,"_right")):
		velocity.x = 1
	return velocity
