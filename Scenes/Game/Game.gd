# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Node

export var characters_count = 4
onready var player_controlled_characters_count = PlayersInfo.players_data.size()
onready var game = $Game
onready var starting_point = $Game/PlayersStartingPoint
var orange = preload("res://Scenes/Character/Orange.tscn")
var viewport_container = preload("res://Scenes/Game/ViewportContainer.tscn")
var players = []
var ticks = 3

func _ready():
	$GridContainer.columns = min(player_controlled_characters_count, 2)
	var pos = starting_point.translation
	for character_id in range(characters_count):
		var new_orange = orange.instance()
		new_orange.translation = pos
		if character_id < player_controlled_characters_count:
			var viewport = viewport_container.instance()
			$GridContainer.add_child(viewport)
			
			new_orange.player_controlled = true
			new_orange.player_data = PlayersInfo.players_data[character_id]
			
			viewport.get_node("Viewport").add_child(new_orange)
		else:
			var dummy_data = PlayerData.new()
			dummy_data.playerId = -1
			new_orange.player_data = dummy_data
			game.add_child(new_orange)
		pos.x += 2
		players.push_back(new_orange)
	
	if player_controlled_characters_count == 3:
		$GridContainer.move_child($GridContainer/CenterContainer, 3)
		$GridContainer/CenterContainer.visible = true


func _on_StartRaceTimer_timeout():
	ticks -= 1
	if (ticks <= 0):
		$StartRaceTimer.stop()
		$Game/AudioStreamPlayer.play()
		$AudioStreamPlayer.stop()
	for player in players:
		if (ticks <= 0):
			player.awake()
		player.get_node("HUD/TrafficLights").tick()
	get_node("Game/LightOnAudio").play()
