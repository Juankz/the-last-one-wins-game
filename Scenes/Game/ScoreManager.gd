# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Node

var finished_players = []

func _ready():
	get_parent().get_node("Game/racetrack/EndLineArea").connect("body_entered",self,"player_crossed_line")
	
func player_crossed_line(body: Node):
	if body is Orange:
		var playerId = body.player_data.playerId
		finished_players.push_back(playerId)
		body.set_as_loser()
		
	if finished_players.size() == 3:
		for player in get_tree().get_nodes_in_group("players"):
			if not finished_players.has((player as Orange).player_data.playerId):
				player.set_as_winner()
		get_tree().paused = true
		get_parent().get_node("GameOverUI").visible = true

