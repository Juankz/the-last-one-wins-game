extends Spatial

var first = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
# Called every frame. 'delta' is the elapsed time since the previous frame.
	var children = get_children()
	var higgestPoint = 0
	for index in children.size():
		var child = children[index] as Orange
		child.speedFactor = 1
		if child.targetPoint > higgestPoint:
			higgestPoint = child.targetPoint
			first = index
	
	children[first].speedFactor = 0.7
