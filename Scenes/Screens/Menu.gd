# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Spatial

var character_selector = preload("res://UI/Nodes/CharacterSelector.tscn")
onready var selector_container: Control = $Control/Container/CenterContainer/VBoxContainer1/GridContainer

func _ready():
	OS.min_window_size = Vector2(1280, 800)
	var players_data: Array = PlayersInfo.players_data
	var count: int = players_data.size()
	
	if (count <= 0):
		var data = add_player_data(1)
		add_player_selector(data)
	else:
		$Control/VBoxContainer/CenterContainer4/PanelContainer/CenterContainer/HBoxContainer/SpinBox.value = count
		for player_data in players_data:
			add_player_selector(player_data)

func start_game():
	get_tree().change_scene("res://Scenes/Game/Game.tscn")

func get_players_count():
	return $Control/VBoxContainer/CenterContainer4/PanelContainer/CenterContainer/HBoxContainer/SpinBox.value

func _on_SpinBox_value_changed(value):
	var players_count = PlayersInfo.players_data.size()
	if value > players_count:
		for i in range(value - players_count):
			var data = add_player_data(players_count + i + 1)
			add_player_selector(data)
	elif value < players_count:
		remove_player_selector(players_count - value)
	$Control/VBoxContainer/CenterContainer4/PanelContainer/CenterContainer/HBoxContainer/SpinBox.get_line_edit().release_focus()

func add_player_data(player_id) -> PlayerData:
	var player_data = PlayerData.new()
	player_data.playerId = player_id
	player_data.skin = player_id - 1
	PlayersInfo.players_data.push_back(player_data)
	return player_data
	
func add_player_selector(player_data: PlayerData):
	var instance = character_selector.instance()
	instance.player_data = player_data
	selector_container.add_child(instance)
	
func remove_player_selector(amount):
	var children: Array = selector_container.get_children()
	for i in range(amount):
		PlayersInfo.players_data.pop_back()
		var child: Node = children.pop_back()
		child.queue_free()

func _on_CreditsButton_pressed():
	$Control/Credits.visible = true
