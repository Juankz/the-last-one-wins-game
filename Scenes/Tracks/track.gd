# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends Node

func _ready():
	setup_iterate(self)

func setup_iterate(node: Node):
	if node is RigidBody:
		node.mode = RigidBody.MODE_STATIC
		node.add_to_group("obstacles")
		node.collision_mask = 2
		node.mass = 100
	for child in node.get_children():
		setup_iterate(child)
