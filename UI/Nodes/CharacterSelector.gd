# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends VBoxContainer

onready var viewport = $CenterContainer2/ViewportContainer/Viewport
var orange = preload("res://Scenes/Character/OrangeDisplay.tscn")
var player_data: PlayerData = null

func _ready():
	assert(player_data != null, "No player data assigned")
	var instance = orange.instance()
	instance.translation = Vector3(player_data.playerId * 2, -10, 0)
	instance.playerID = player_data.playerId
	instance.skin = player_data.skin
	viewport.add_child(instance)
	$Buttons/PrevCharacter.connect("pressed", instance, "prev_character")
	$Buttons/Label.text = str("P",player_data.playerId)
	$Buttons/NextCharacter.connect("pressed", instance, "next_character")
	
	if player_data.playerId > 1:
		$Buttons/PrevCharacter.icon = null
		$Buttons/NextCharacter.icon = null
		match player_data.playerId:
			2:
				$Buttons/PrevCharacter.text = "Z"
				$Buttons/NextCharacter.text = "X"
			3:
				$Buttons/PrevCharacter.text = "N"
				$Buttons/NextCharacter.text = "M"
			4:
				$Buttons/PrevCharacter.text = "Q"
				$Buttons/NextCharacter.text = "W"
