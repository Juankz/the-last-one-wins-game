extends AudioStreamPlayer

func _on_button_pressed():
	play()

func _on_SpinBox_gui_input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			play()
