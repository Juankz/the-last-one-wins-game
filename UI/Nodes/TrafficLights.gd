# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
extends HBoxContainer

var times = 3

var yellow_light: Texture = preload("res://UI/Textures/traffic_light_yellow.png")
var green_light: Texture = preload("res://UI/Textures/traffic_light_green.png")

func tick():
	times -= 1
	match times:
		0:
			$HideTimer.start()
			$TextureRect3.texture = green_light
		1:
			$TextureRect2.texture = yellow_light
		2:
			$TextureRect.texture = yellow_light
		_:
			pass


func _on_HideTimer_timeout():
	visible = false
